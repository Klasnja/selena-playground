package com.workyard.app.config

class AppConfig {
    object NetworkConfig {
        val URL = "https://earthquake.usgs.gov/fdsnws/"
    }
}