package com.workyard.app.presentation.detail

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.workyard.app.presentation.detail.fragment.DetailFragment
import com.workyard.app.presentation.detail.fragment.DetailFragmentModule


@Module
interface DetailFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(DetailFragmentModule::class))
    fun provideDetailFragment(): DetailFragment
}
