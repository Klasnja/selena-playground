package com.workyard.app.presentation.detail

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.workyard.app.R
import com.workyard.app.domain.models.Earthquake
import com.workyard.app.presentation.common.BaseActivity
import com.workyard.app.presentation.detail.fragment.DetailFragment


class DetailActivity : BaseActivity<DetailPresenter>(), DetailPresenter.View {
    override fun showError(throwable: Throwable) {
        val text = "\"Error: ${throwable.message}"
        val duration = Toast.LENGTH_SHORT

        val toast = Toast.makeText(applicationContext, text, duration)
        toast.show()
    }

    override fun showDone(earthquakes: Map<String, Earthquake>) {
        val toast = Toast.makeText(applicationContext, "Loaded done", Toast.LENGTH_SHORT)
        toast.show()
    }

    override fun getResId() = R.layout.activity_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter.loadDetail()

        if (savedInstanceState == null) {
            val fragment = DetailFragment.newInstance()
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.container, fragment, fragment.TAG)
                    .commitAllowingStateLoss()
        }
    }

    override fun onDetailLoaded() {
        Log.v("TEST", "Detail is loaded")
    }

    override fun isHomeButtonEnabled() = true

    override fun getToolbarTitle() : String = getString(R.string.activity_details)

}
