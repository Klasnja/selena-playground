package com.workyard.app.presentation.common

import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.view.MenuItem
import com.workyard.app.R
import com.workyard.app.util.RxEventBus
import com.workyard.app.util.Schedulers
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.layout_app_bar.*
import javax.inject.Inject


abstract class BaseActivity<PresenterType : BasePresenter<BasePresenter.BaseView>> : DaggerAppCompatActivity(), BasePresenter.BaseView {

    @Inject
    lateinit var presenter: PresenterType

    @Inject
    lateinit var eventBus: RxEventBus

    @Inject
    lateinit var schedulers: Schedulers

    private var saveState: Bundle? = null

    companion object {
        private const val SAVED_STATE = "SAVED_STATE"
    }

    protected val tasksWhileInMemory = CompositeDisposable()
    protected val tasksWhileVisible = CompositeDisposable()
    protected val tasksWhileActive = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadSavedState(savedInstanceState)
        setContentView(getResId())
        setupToolbar()
    }

    private fun loadSavedState(savedInstanceState: Bundle?) {
        saveState = if (savedInstanceState != null && savedInstanceState.containsKey(SAVED_STATE)) {
            savedInstanceState.getBundle(SAVED_STATE)
        } else {
            Bundle()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBundle(SAVED_STATE, saveState)
    }

    private fun setupToolbar() {
        toolbar?.apply {
            title = getToolbarTitle()
            setSupportActionBar(this)
            if (isHomeButtonEnabled()) {
                supportActionBar?.apply {
                    setHomeButtonEnabled(true)
                    setDisplayHomeAsUpEnabled(true)
                    setHomeAsUpIndicator(getHomeButtonIndicatorDrawable())
                }
            }
        }
    }

    @LayoutRes
    protected abstract fun getResId(): Int

    abstract fun isHomeButtonEnabled(): Boolean

    protected open fun getToolbarTitle() = ""

    @DrawableRes
    protected open fun getHomeButtonIndicatorDrawable() = R.drawable.ic_arrow_back_24dp

    override fun onPause() {
        super.onPause()
        tasksWhileActive.clear()
        presenter.onViewPause()
    }

    override fun onStop() {
        super.onStop()
        tasksWhileVisible.clear()
        presenter.onViewStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        tasksWhileInMemory.clear()
        presenter.onViewDestroy()
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            //android.R.id.search -> searchButton()
        }

        return super.onOptionsItemSelected(menuItem)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onBackPressed() {
        finish()
    }

    override fun showLoader() {

    }

    override fun hideLoader() {

    }
}