package com.workyard.app.presentation.common

import com.workyard.app.util.Logger
import com.workyard.app.util.Schedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


open class BasePresenter<out ViewType : BasePresenter.BaseView> constructor(protected val view: ViewType){
    @Inject
    lateinit var logger: Logger

    @Inject
    lateinit var schedulers: Schedulers

    protected val tasksWhileInMemory = CompositeDisposable()
    protected val tasksWhileVisible = CompositeDisposable()
    protected val tasksWhileActive = CompositeDisposable()

    open fun onViewPause() {
        tasksWhileActive.clear()
    }

    open fun onViewStop() {
        tasksWhileVisible.clear()
    }

    open fun onViewDestroy() {
        tasksWhileInMemory.clear()
    }

    interface BaseView {
        fun showLoader()
        fun hideLoader()
    }
}