package com.workyard.app.presentation.main

import dagger.Binds
import dagger.Module


@Module
interface MainActivityModule {

    @Binds
    fun provideView(mainActivity: MainActivity): MainPresenter.View

    fun provideMainPresenter(): MainPresenter
}
