package com.workyard.app.presentation.detail

import dagger.Binds
import dagger.Module


@Module
interface DetailActivityModule {

    @Binds
    fun provideView(detailActivity: DetailActivity): DetailPresenter.View

    fun provideDetailPresenter(): DetailPresenter
}
