package com.workyard.app.presentation.common

import android.os.Bundle
import android.view.View
import com.workyard.app.util.RxEventBus
import com.workyard.app.util.Schedulers
import dagger.android.support.DaggerFragment
import javax.inject.Inject


abstract class BaseFragment<PresenterType: BasePresenter<BasePresenter.BaseView>> : DaggerFragment(), BasePresenter.BaseView {

    @Inject
    lateinit var presenter: PresenterType

    @Inject
    lateinit var eventBus: RxEventBus

    @Inject
    lateinit var schedulers: Schedulers

    private var saveState: Bundle? = null

    val TAG : String = this::class.java.simpleName

    companion object {
        private const val SAVED_STATE = "SAVED_STATE"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadSavedState(savedInstanceState)
    }

    private fun loadSavedState(savedInstanceState: Bundle?) {
        saveState = if (savedInstanceState != null && savedInstanceState.containsKey(SAVED_STATE)) {
            savedInstanceState.getBundle(SAVED_STATE)
        } else {
            Bundle()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBundle(SAVED_STATE, saveState)
    }

    override fun showLoader() {

    }

    override fun hideLoader() {

    }

    data class DismissFragment(val tag: String)
}