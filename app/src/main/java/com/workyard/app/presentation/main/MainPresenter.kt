package com.workyard.app.presentation.main

import com.workyard.app.presentation.common.BasePresenter
import javax.inject.Inject

class MainPresenter @Inject constructor(view: View) : BasePresenter<MainPresenter.View>(view) {

    fun loadMain() {

    }

    interface View : BaseView {
        fun onMainLoaded()
    }
}

