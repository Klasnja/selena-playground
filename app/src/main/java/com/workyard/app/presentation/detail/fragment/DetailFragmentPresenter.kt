package com.workyard.app.presentation.detail.fragment

import com.workyard.app.presentation.common.BasePresenter
import com.workyard.app.presentation.detail.DetailPresenter
import javax.inject.Inject


class DetailFragmentPresenter @Inject constructor(view: View, private val detailPresenter: DetailPresenter) : BasePresenter<DetailFragmentPresenter.View>(view) {

    init {
        view.onDetailFragmentLoaded()
    }

    interface View : BaseView {
        fun onDetailFragmentLoaded()
    }
}
