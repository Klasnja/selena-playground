package com.workyard.app.presentation.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.workyard.app.R
import com.workyard.app.presentation.common.BaseActivity
import com.workyard.app.presentation.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainPresenter>(), MainPresenter.View {

    override fun getResId() = R.layout.activity_main

    override fun getToolbarTitle() : String = getString(R.string.dashboard)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter.loadMain()

        button.setOnClickListener {
            startActivity(Intent(this, DetailActivity::class.java))
        }
    }

    override fun onMainLoaded() {
        Log.v("TEST", "Main page is loaded.")
    }

    override fun isHomeButtonEnabled() = false
}
