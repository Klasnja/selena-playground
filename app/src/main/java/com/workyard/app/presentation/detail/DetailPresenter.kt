package com.workyard.app.presentation.detail

import com.workyard.app.domain.models.Earthquake
import com.workyard.app.domain.usecases.UseCase
import com.workyard.app.presentation.common.BasePresenter
import javax.inject.Inject

class DetailPresenter @Inject constructor( view: View,
        private val useCase: UseCase) : BasePresenter<DetailPresenter.View>(view) {

    companion object {
        private val TAG = DetailPresenter::class.java.simpleName
    }

    fun loadDetail() {
        view.showLoader()
        tasksWhileInMemory.add(useCase.loadData()
                .observeOn(schedulers.ui())
                .subscribe({
                    view.onDetailLoaded()
                    view.showDone(it)
                    view.hideLoader()
                }, {
                    view.showError(it)
                    logger.error(TAG, it.stackTrace.toString(), it)
                    view.hideLoader()
                }))
    }

    interface View : BaseView {
        fun onDetailLoaded()
        fun showError(throwable: Throwable)
        fun showDone(earthquakes: Map<String, Earthquake>)
    }
}
