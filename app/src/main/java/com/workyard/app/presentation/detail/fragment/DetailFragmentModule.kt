package com.workyard.app.presentation.detail.fragment

import dagger.Binds
import dagger.Module


@Module
interface DetailFragmentModule {

    @Binds
    fun provideView(detailFragment: DetailFragment): DetailFragmentPresenter.View
}
