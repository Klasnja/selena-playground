package com.workyard.app.domain.models

data class Earthquake(val x: Double,
                      val y: Double,
                      val z: Double)
