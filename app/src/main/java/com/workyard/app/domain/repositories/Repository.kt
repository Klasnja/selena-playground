package com.workyard.app.domain.repositories

import com.workyard.app.domain.models.Earthquake
import io.reactivex.Single

interface Repository {
    fun getEarthquakeMap(): Single<Map<String, Earthquake>>
}