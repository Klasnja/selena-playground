package com.workyard.app.domain.usecases

import com.workyard.app.domain.models.Earthquake
import com.workyard.app.domain.repositories.Repository
import com.workyard.app.util.Schedulers
import io.reactivex.Single
import javax.inject.Inject

class UseCase @Inject constructor(private val repository: Repository,
                                  private val schedulers: Schedulers) {
    fun loadData() : Single<Map<String, Earthquake>> {
        return repository.getEarthquakeMap()
                .observeOn(schedulers.ui())
    }
}