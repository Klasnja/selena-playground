package com.workyard.app.injection

import android.app.Application
import android.content.Context
import com.workyard.app.data.managers.Manager
import com.workyard.app.domain.repositories.Repository
import com.workyard.app.util.Logger
import com.workyard.app.util.RxEventBus

import dagger.Binds
import dagger.Module
import dagger.Reusable
import javax.inject.Singleton

@Module
interface AppModule {

    @Binds
    fun provideContext(application: Application): Context

    @Binds
    fun provideManager(manager: Manager): Repository

    @Reusable
    fun provideLogger(): Logger

    @Singleton
    fun provideEventBus(): RxEventBus
}
