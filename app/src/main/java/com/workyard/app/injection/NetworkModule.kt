package com.workyard.app.injection

import com.workyard.app.config.AppConfig
import com.workyard.app.data.api.services.ApiService
import com.workyard.app.util.Schedulers
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class NetworkModule {

    companion object {
        val URL = "https://earthquake.usgs.gov/fdsnws/"
    }

    @Singleton
    @Provides
    fun provideOkHttp(): OkHttpClient {
        val builder = OkHttpClient().newBuilder()
        return builder.build()
    }


    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient,
                        schedulers: Schedulers): Retrofit.Builder = buildRetrofit(okHttpClient, schedulers)

    private fun buildRetrofit(okHttpClient: OkHttpClient,
                              schedulers: Schedulers): Retrofit.Builder {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(AppConfig.NetworkConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(schedulers.io()))

    }

    @Singleton
    @Provides
    fun provideApiService(retrofitBuilder : Retrofit.Builder): ApiService = buildAppApi(retrofitBuilder, ApiService::class.java)

    private fun <T> buildAppApi(retrofitBuilder: Retrofit.Builder, service: Class<out T>): T =
            retrofitBuilder
                    .build()
                    .create(service)

}