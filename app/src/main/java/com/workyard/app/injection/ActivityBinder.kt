package com.workyard.app.injection

import com.workyard.app.presentation.detail.DetailActivity
import com.workyard.app.presentation.detail.DetailActivityModule
import com.workyard.app.presentation.detail.DetailFragmentProvider
import com.workyard.app.presentation.main.MainActivity
import com.workyard.app.presentation.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
interface ActivityBinder {

    @ContributesAndroidInjector(modules = arrayOf(MainActivityModule::class))
    fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = arrayOf(DetailActivityModule::class, DetailFragmentProvider::class))
    fun bindDetailActivity(): DetailActivity
}
