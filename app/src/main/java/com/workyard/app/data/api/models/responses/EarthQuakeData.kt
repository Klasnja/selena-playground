package com.workyard.app.data.api.models.responses

data class EarthQuakes(val features: List<Feature>)

data class Feature(val properties: Properties, val geometry: Geometry)

data class Geometry(val coordinates: List<Double>, val time: String)

data class Properties(val place: String, val time: Long)

