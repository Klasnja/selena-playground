package com.workyard.app.data.api.services

import com.workyard.app.data.api.models.responses.EarthQuakes
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers


interface ApiService {
    @Headers("Content-Type:application/json")
    @GET("event/1/query?format=geojson&starttime=2014-01-01&endtime=2014-01-02")
    fun getEarthquakes() : Single<EarthQuakes>

}
