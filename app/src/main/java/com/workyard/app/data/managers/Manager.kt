package com.workyard.app.data.managers

import com.workyard.app.data.api.services.ApiService
import com.workyard.app.data.mappers.Mapper
import com.workyard.app.domain.models.Earthquake
import com.workyard.app.domain.repositories.Repository
import io.reactivex.Single
import javax.inject.Inject

class Manager @Inject constructor(private val apiService: ApiService): Repository {

    override fun getEarthquakeMap(): Single<Map<String, Earthquake>> {
        return  apiService.getEarthquakes().map { it ->
            val earthquakeMap = mutableMapOf<String, Earthquake>()
            it.features.forEach { feature ->
                earthquakeMap.put(feature.properties.place,
                        Mapper.mapFeatureToEarthquake(feature))
            }
            earthquakeMap
        }
    }
}