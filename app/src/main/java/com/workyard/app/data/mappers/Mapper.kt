package com.workyard.app.data.mappers

import com.workyard.app.data.api.models.responses.Feature
import com.workyard.app.domain.models.Earthquake

class Mapper {
    companion object {
        fun mapFeatureToEarthquake(feature: Feature): Earthquake =
                Earthquake(feature.geometry.coordinates[0],
                        feature.geometry.coordinates[1],
                        feature.geometry.coordinates[2]
                )
    }
}