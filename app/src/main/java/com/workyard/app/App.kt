package com.workyard.app

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import com.workyard.app.injection.DaggerAppComponent


class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }
}
