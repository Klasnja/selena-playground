package com.workyard.app.util

import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class Schedulers @Inject constructor() {
    open fun ui(): Scheduler {
        return io.reactivex.android.schedulers.AndroidSchedulers.mainThread()
    }

    open fun io(): Scheduler {
        return io.reactivex.schedulers.Schedulers.io()
    }

    open fun single(): Scheduler {
        return io.reactivex.schedulers.Schedulers.single()
    }

    open fun computation(): Scheduler {
        return io.reactivex.schedulers.Schedulers.computation()
    }

    open fun trampoline(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }
}
