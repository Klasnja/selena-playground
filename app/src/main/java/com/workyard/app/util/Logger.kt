package com.workyard.app.util

import android.util.Log
import javax.inject.Inject

class Logger @Inject constructor() {
    fun debug(tag: String, message: String?) {
        Log.d(tag, message)
    }

    fun error(tag: String, message: String?) {
        Log.e(tag, message)
    }

    fun error(tag: String, message: String?, throwable: Throwable) {
        Log.e(tag, message, throwable)
    }

    fun verbose(tag: String, message: String?) {
        Log.v(tag, message)
    }
}