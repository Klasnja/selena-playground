package com.workyard.app.util

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RxEventBus @Inject constructor() {

    private val bus = PublishSubject.create<Any>()

    fun publish(event: Any) {
        bus.onNext(event)
    }

    fun <T> listenTo(clazz: Class<T>): Observable<T> {
        return bus.ofType(clazz)
    }

    fun hasObservers(): Boolean {
        return bus.hasObservers()
    }
}